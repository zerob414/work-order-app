# Help Desk Work Order App

This is a side project I created in my spare time for tracking work orders at the Utah National Guard's Help Desk. The back end is written in PowerShell and the front end is HTML/CSS/JavaScript with JQuery and JQueryUI. The application is intended to be single user and only accessible on `localhost:8080`.

Note: This project is a personal project that is not sponsored or endorsed by the Utah National Guard in any way. I have not and will not receive compensation for any part or the whole of this application. The Utah National Guard is free to use and modify this code in any way they wish without compensation or notification.

## Why This Project
Working at the Help Desk was not my main job, but while I was there, I was asked to help improve their current application for creating and tracking work orders. The application in place at the time was written in Microsoft Access with VBA and though it accomplished it's job, it was woefully outdated and difficult to modernize. I built a new but similar application for them, again in Microsoft Access (with a lot of Visual Basic for Applications (VBA) code). It worked, but ultimately was not adopted for much the same reasons they were trying to replace the original application.

## Why PowerShell
Feeling as though I had failed my objective, I started to research other ways I could solve the problem. Java is my main and most proficient language (although I now prefer Python for scripts and quick applications), so I first looked into building a Java application with a JavaFX front end. I found that Java was heavily frowned upon on military networks and it would be difficult to get any Java application I wrote approved for use. My next thought was C# with a Windows Presentation Foundation (WPF) frontend. I ran into a similar issue in that any compiled applications to be run on military systems must go through a stringent approval process at high levels that include source code reviews, testing and a bunch of other stuff. On the other hand, non-compiled scripts could be run with local reviews and approvals, making the process much easier and quicker. So I settled on using PowerShell. Originally, I tried to use WPF with PowerShell, but I found that PowerShell did a bunch of weird stuff when parsing the XML files used to build WPF interfaces. I also tried Windows Forms, but interfacing that UI with PowerShell and making it look good is very tedious. I eventually found a tutorial for making a webserver with PowerShell and decided on a good old HTML5/CSS3/JavaScript frontend, with JQuery and JQueryUI frameworks to make things go a tad bit quicker. 

## Where's the Database?
The application uses a Microsoft Access database file for persistant storage. It is not included in this repo because it contains some info I would rather not be public. A diagram of the database schema is included, howerver.

Why Microsoft Access? It is available on the military computers without the need to install any extra applications, drivers or services. If I had used Java, the database would be a JavaDB/Derby file. If I had used C# it would be a Microsoft SQL Server Compact file.

## Other Framworks and Modules Used
The following are used in this project:
- JQuery
- JQueryUI
- FontAwesome
- iTextSharp

I claim no credit for these frameworks and modules, they are the property of their respective owners.

## Executing
You will need to download jQuery, jQueryUI, and FontAwesome to execute this project. Reference the `filetree.txt` file to see what your directory structure should look like. If it doesn't match, you won't be able to execute the project (the library versions can probably be updated). Once your directory structure matches, simply open a PowerShell terminal and execute `webserver.ps1`.