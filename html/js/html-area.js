(function( $ ) {
    
       $.fn.htmlArea = function() {
    
           this.filter( "textarea" ).each(function() {
               var link = $( this );
               link.append( " (" + link.attr( "href" ) + ")" );
           });
    
           return this;
    
       };
    
   }( jQuery ));