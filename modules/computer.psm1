Using Module .\database.psm1

class ComputerProfile {
    $databasePath = ".\db\WorkOrderDB.accdb"
    $database = $null
    $html = ""

    ComputerProfile(){}
    ComputerProfile($databasePath){
        $this.databasePath = $databasePath
        $this.database = [Database]::new($databasePath)
    }
    ComputerProfile($databasePath, $parameters){
        $this.databasePath = $databasePath
        $this.database = [Database]::new($databasePath)
        foreach ($par in $parameters.GetEnumerator()) {
            switch ($par.key) {
                "lookup" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM (Tbl_Computers INNER JOIN Tbl_ComputerModels ON Tbl_Computers.MODEL = Tbl_ComputerModels.ID) INNER JOIN Tbl_DirectorateCodes ON Tbl_DirectorateCodes.ID = Tbl_Computers.OWNER WHERE SERVICE_TAG LIKE '%$val%' OR COMPUTER_NAME LIKE '%$val%'"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    Write-Host "Records Returned: " $RecSet.RecordCount
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    elseif($RecSet.RecordCount -eq 1){
                        $RecSet.MoveFirst()
                        $compType = "Desktop"
                        if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ $compType = "Laptop" }
                        $model = -join($RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("Tbl_ComputerModels.MODEL").Value, " ", $compType)
                        $ondomain = "No"
                        if($RecSet.Fields.Item("ON_DOMAIN").Value -eq 1){ $ondomain = "Yes" }
                        $lastservice = $RecSet.Fields.Item("LAST_SERVICED").Value
                        if($lastservice -as [datetime]){
                            $ts = New-TimeSpan -Start $lastservice -End (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($lastservice)
                            $lastservice =  get-date $date -Format "d MMM yyyy"
                            $lastservice = "$lastservice ($days days ago)"
                        }
                        else{$lastservice = "(unknown)"}
                        $warrantyexp = $RecSet.Fields.Item("WARRANTY_EXPIRATION").Value
                        if($warrantyexp -as [DateTime]){
                            $ts = New-TimeSpan -End $warrantyexp -Start (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($warrantyexp)
                            $warrantyexp =  get-date $date -Format "d MMM yyyy"
                            if($days -ge 0){
                                $warrantyexp = "$warrantyexp ($days days from now)"
                            }
                            else{
                                $days = [math]::abs($days)
                                $warrantyexp = "$warrantyexp (Expired $days days ago)"
                            }
                        }
                        else{$warrantyexp = "(unknown)"}
                        $vars = @{
                            model = $this.d($model, "(Model Unknown)")
                            comptype = $this.d($compType, "laptop")
                            computername = $this.d($RecSet.Fields.Item("COMPUTER_NAME").Value, "(Computer Name Unknown)")
                            servicetag = $this.d($RecSet.Fields.Item("SERVICE_TAG").Value, "(Unknown)")
                            owner = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown)")
                            ondomain = $this.d($ondomain, "(Unknown)")
                            lastproblem = $this.d($RecSet.Fields.Item("LAST_PROBLEM").Value, "(none)")
                            description = $this.d($RecSet.Fields.Item("DESCRIPTION").Value, "(none)")
                            lastservice = $lastservice
                            warrantyexpiration = $warrantyexp
                        }
                        $this.html = (Read-Html "computer-body.html" $vars)
                    }
                    else{
                        $RecSet.MoveFirst()
                        $this.html = "<div class='list'>"
                        do{
                            $computername = $RecSet.Fields.Item("COMPUTER_NAME").Value
                            $compType = "Desktop"
                            if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ $compType = "Laptop" }    
                            $model = -join($RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("Tbl_ComputerModels.MODEL").Value, " ", $compType)
                            $owner = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown Owner)")
                            $servicetag = $this.d($RecSet.Fields.Item("SERVICE_TAG").Value, "Unknown Service Tag")
                            $this.html += "<div class='list-item'>"
                            $this.html += " <a href=`"javascript:lookupComputer('$servicetag')`"><i class='fas fa-laptop'></i>"
                            $this.html += "  $computername ($servicetag) $model owned by $owner"
                            $this.html += " </a></div>"
                            $RecSet.MoveNext()
                        }while(-not $RecSet.EOF)
                        $this.html += "</div>"
                    }
                    $RecSet.Close()
                }
                "lookupforedit" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM Tbl_Computers WHERE SERVICE_TAG = `"$val`""
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $vars = @{
                            model = $RecSet.Fields.Item("MODEL").Value
                            computername = $RecSet.Fields.Item("COMPUTER_NAME").Value
                            servicetag = $RecSet.Fields.Item("SERVICE_TAG").Value
                            owner = $RecSet.Fields.Item("OWNER").Value
                            ondomain = $RecSet.Fields.Item("ON_DOMAIN").Value
                            lastproblem = $RecSet.Fields.Item("LAST_PROBLEM")
                            description = $RecSet.Fields.Item("DESCRIPTION").Value
                            lastservice = $RecSet.Fields.Item("LAST_SERVICED").Value
                            warrantyexpiration = $RecSet.Fields.Item("WARRANTY_EXPIRATION").Value
                        }
                        $this.html = ($vars | convertTo-Json)
                    }
                    $RecSet.Close()
                }
                "delete"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        $query = "DELETE FROM Tbl_Computers WHERE SERVICE_TAG = '$val'"
                        $this.database.execute($query)
                        # Check if delete was successful
                        $query = "SELECT * FROM Tbl_Computers WHERE SERVICE_TAG = `"$val`""
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.EOF -OR $RecSet.State -eq 0){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }
                "getmodels"{
                    $query = "SELECT * FROM Tbl_ComputerModels ORDER BY MANUFACTURER, MODEL"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {
                            $compType = "Desktop"
                            $compTypeAbbr = "WK"
                            if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ 
                                $compType = "Laptop" 
                                $compTypeAbbr = "NB"
                            }    
                            $this.html = -join($this.html, '<option value="', $RecSet.Fields.Item("ID").Value, '" typeabbreviation="', $compTypeAbbr ,'">', $RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("MODEL").Value, " (", $compType, ')</option>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "getdirectorates"{
                    $query = "SELECT * FROM Tbl_DirectorateCodes ORDER BY [SortOrder]"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {
                            $this.html = -join($this.html, '<option value="', $RecSet.Fields.Item("ID").Value, '" code="', $RecSet.Fields.Item("DirectorateCode").Value ,'">', $RecSet.Fields.Item("DirectorateDesc").Value, '</option>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }

                Default {}
            }
        }
    }
    updateComputer($data){
        $servicetag = $this.s($data.item('servicetag'))
        $query = "SELECT * FROM Tbl_Computers WHERE SERVICE_TAG = `"$servicetag`""
        $RecSet = $this.database.openRecordSet($query, 3, 1)
        if($RecSet.State -gt 0){ # Make sure the record set opened
            if($RecSet.EOF){  # If 0 records are returned, we'll insert
                $query = "INSERT INTO Tbl_Computers (SERVICE_TAG, MODEL, OWNER, ON_DOMAIN, COMPUTER_NAME, DESCRIPTION, WARRANTY_EXPIRATION) VALUES ("
                $query = -join($query, "'", $this.s($data.item('servicetag')), "', '", $this.s($data.item('model')), "', '", $this.s($data.item('owner')), "', ", $this.s($data.item('ondomain')), ", '", $this.s($data.item('computername')), "', '")
                $query = -join($query, $this.s($data.item('description')), "', '", $this.s($data.item('warrantyexpiration')), "')")
                Write-Host "query = $query"
                $this.database.execute($query)
            }
            else{  #if a record is returned, we'll update
                $query = -join("UPDATE Tbl_Computers SET SERVICE_TAG='", $this.s($data.item('servicetag')),"', MODEL='", $this.s($data.item('model')), "', OWNER='",  $this.s($data.item('owner')), "', ON_DOMAIN=", $this.s($data.item('ondomain')))
                $query = -join($query, ", COMPUTER_NAME='", $this.s($data.item('computername')), "', DESCRIPTION='", $this.s($data.item('description')), "', WARRANTY_EXPIRATION='", $this.s($data.item('warrantyexpiration')))
                $query = -join($query, "' WHERE SERVICE_TAG='$servicetag'") 
                Write-Host "query = $query"
                $this.database.execute($query)
            }
        }
    }
    [String] getHtml(){
        return $this.html
    }
    cleanup(){
        $this.database.cleanup()
    }
    [string] d($str, $def){
        # Sets a string to a default if it is empty
        if([String]::IsNullOrEmpty($str)){
            $str = $def
        }
        return $str
    }
    [string] s($str){
        # Convenience wrapper
        return $this.database.sanitizeString($str)
    }
    [string] formatPhone($str){
        if($str -match '\d{4,11}'){
            switch($str.length){
                11{
                    $str = $str.insert(1," ")
                    $str = $str.insert(4,"-")
                    $str = $str.insert(8,"-")
                }
                10{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                9{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                8{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                7{
                    $str = $str.insert(3,"-")
                }
                6{
                    $str = $str.insert(3,"-")
                }
                5{
                    $str = $str.insert(3,"-")
                 }
                4{
                    $str = $str.insert(3,"-")
                }
            }
        }
        return $str
    }
}