Using Module .\database.psm1
Using Module  .\ConvertFrom-HTMLtoPDF.ps1

class WorkOrders {
    $databasePath = ".\db\WorkOrderDB.accdb"
    $database = $null
    $html = ""

    WorkOrders(){}
    WorkOrders($databasePath){
        $this.databasePath = $databasePath
        $this.database = [Database]::new($databasePath)
    }
    WorkOrders($databasePath, $parameters){
        $this.databasePath = $databasePath
        $this.database = [Database]::new($databasePath)
        foreach ($par in $parameters.GetEnumerator()) {
            switch ($par.key) {
                "getworkorders" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM (Tbl_WorkOrders INNER JOIN Tbl_Users ON Tbl_WorkOrders.USER_ID = Tbl_Users.ID) INNER JOIN Tbl_Ranks ON Tbl_Ranks.ID = Tbl_Users.RANK WHERE Tbl_Workorders.ID LIKE '%$val%' OR F_NAME LIKE '%$val%' OR L_NAME LIKE '%$val%' ORDER BY Tbl_Workorders.CREATED DESC"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    Write-Host "Records Returned: " $RecSet.RecordCount
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = "No Work Orders Exist Yet."
                    }                    
                    $RecSet.MoveFirst()
                    #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                    # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                    $repetitions = 0 
                    do { 
                        $created = $RecSet.Fields.Item("Tbl_Workorders.CREATED").Value
                        if($created -as [datetime]){
                            $ts = New-TimeSpan -Start $created -End (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($created)
                            $created =  get-date $date -Format f
                            $created = "$created ($days days ago)"
                        }
                        else{$created = "(unknown)"}
                        $number = $RecSet.Fields.Item("Tbl_Workorders.ID").Value
                        $name = -join($RecSet.Fields.Item("Tbl_Ranks.RANK").Value, " ", $RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("MI").Value, " ", $RecSet.Fields.Item("L_NAME").Value)                        
                        $status = "New"
                        switch($RecSet.Fields.Item("STATUS").Value){
                            1{
                                $status = "In Progress"
                            }
                            2{
                                $status = "Finished"
                            }
                        }

                        $vars = @{
                            number = $this.d($number, "(UNK)")
                            name = $this.d($name, "(Unknown)")
                            created = $this.d($created, "(Unknown)")
                            status = $this.d($status, "(Unknown)")
                            userid = $this.d($RecSet.Fields.Item("Tbl_Users.ID").Value, "#")
                        }
                        $this.html += (Read-Html "workorder-list-row.html" $vars)
                        
                        $RecSet.MoveNext()
                        $repetitions++  #Do not remove this line! Infinite loops will ensue!
                    } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.

                    $RecSet.Close()
                }
                "lookupuser" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM (Tbl_Users INNER JOIN Tbl_Ranks ON Tbl_Users.RANK = Tbl_Ranks.ID) INNER JOIN Tbl_DirectorateCodes ON Tbl_DirectorateCodes.ID = Tbl_Users.DIRECTORATE WHERE USERNAME LIKE `"%$val%`" OR L_NAME LIKE '%$val%' OR F_NAME LIKE '%$val%'"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    Write-Host "Records Returned: " $RecSet.RecordCount
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    elseif($RecSet.RecordCount -eq 1){
                        $RecSet.MoveFirst()
                        $name = -join($RecSet.Fields.Item("Tbl_Ranks.RANK").Value, " ", $RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("L_NAME").Value)
                        $isAdmin = "";
                        if($RecSet.Fields.Item("IsAdmin").Value -eq 1){$isAdmin = "<b>Administrator</b>"}
                        $created = $RecSet.Fields.Item("CREATED").Value
                        if($created -as [datetime]){
                            $ts = New-TimeSpan -Start $created -End (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($created)
                            $created =  get-date $date -Format "d MMM yyyy"
                            $created = "$created ($days days ago)"
                        }
                        else{$created = "(unknown)"}
                        $lastUsed = $RecSet.Fields.Item("LAST_USED").Value
                        if($lastUsed -as [DateTime]){
                            $ts = New-TimeSpan -Start $lastUsed -End (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($lastUsed)
                            $lastUsed =  get-date $date -Format "d MMM yyyy"
                            $lastUsed = "$lastUsed ($days days ago)"
                        }
                        else{$lastUsed = "(never)"}
                        $vars = @{
                            id = $this.d($RecSet.Fields.Item("Tbl_Users.ID").Value, "-1")
                            name = $this.d($name, "(Unnamed)")
                            unit = $this.d($RecSet.Fields.Item("UNIT").Value, "(Unit Unknown)")
                            username = $this.d($RecSet.Fields.Item("USERNAME").Value, "(Username Unknown)")
                            workPhone = $this.formatPhone($this.d($RecSet.Fields.Item("WORK_PHONE").Value, "(none)"))
                            cellPhone = $this.formatPhone($this.d($RecSet.Fields.Item("CELL_PHONE").Value, "(none)"))
                            email = $this.d($RecSet.Fields.Item("EMAIL").Value, "(none)")
                            employment = $this.d($RecSet.Fields.Item("EMPLOYMENT").Value, "(Unknown Employment Status)")
                            directorate = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown Directorate)")
                            location = $this.d($RecSet.Fields.Item("LOCATION").Value, "(Unknown Location)")
                            created = $created
                            lastUsed = $lastUsed
                            isAdmin = $isAdmin
                        }
                        $this.html = (Read-Html "profile-body-compact.html" $vars)
                    }
                    else{
                        $RecSet.MoveFirst()
                        $this.html = "<div class='list'>"
                        do{
                            $username = $RecSet.Fields.Item("USERNAME").Value
                            $name = -join($RecSet.Fields.Item("Tbl_Ranks.RANK").Value, " ", $RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("L_NAME").Value)
                            $employment = $this.d($RecSet.Fields.Item("EMPLOYMENT").Value, "(Unknown Employment Status)")
                            $directorate = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown Directorate)")
                            $location = $this.d($RecSet.Fields.Item("LOCATION").Value, "(Unknown Location)")
                            $this.html += "<div class='list-item'>"
                            $this.html += " <a href=`"javascript:lookupUser('$username')`"><i class='fas fa-user'></i>"
                            $this.html += "  $name ($username) $employment at $directorate in $location"
                            $this.html += " </a></div>"
                            $RecSet.MoveNext()
                        }while(-not $RecSet.EOF)
                        $this.html += "</div>"
                    }
                    $RecSet.Close()
                }
                "lookupcomputer" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM (Tbl_Computers INNER JOIN Tbl_ComputerModels ON Tbl_Computers.MODEL = Tbl_ComputerModels.ID) INNER JOIN Tbl_DirectorateCodes ON Tbl_DirectorateCodes.ID = Tbl_Computers.OWNER WHERE SERVICE_TAG LIKE '%$val%' OR COMPUTER_NAME LIKE '%$val%'"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    Write-Host "Records Returned: " $RecSet.RecordCount
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    elseif($RecSet.RecordCount -eq 1){
                        $RecSet.MoveFirst()
                        $compType = "Desktop"
                        if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ $compType = "Laptop" }
                        $model = -join($RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("Tbl_ComputerModels.MODEL").Value, " ", $compType)
                        $ondomain = "<i>Off Domain</i>"
                        if($RecSet.Fields.Item("ON_DOMAIN").Value -eq 1){ $ondomain = "On Domain" }
                        $lastservice = $RecSet.Fields.Item("LAST_SERVICED").Value
                        if($lastservice -as [datetime]){
                            $ts = New-TimeSpan -Start $lastservice -End (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($lastservice)
                            $lastservice =  get-date $date -Format "d MMM yyyy"
                            $lastservice = "$lastservice ($days days ago)"
                        }
                        else{$lastservice = "(unknown)"}
                        $warrantyexp = $RecSet.Fields.Item("WARRANTY_EXPIRATION").Value
                        if($warrantyexp -as [DateTime]){
                            $ts = New-TimeSpan -End $warrantyexp -Start (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($warrantyexp)
                            $warrantyexp =  get-date $date -Format "d MMM yyyy"
                            if($days -ge 0){
                                $warrantyexp = "$warrantyexp ($days days from now)"
                            }
                            else{
                                $days = [math]::abs($days)
                                $warrantyexp = "$warrantyexp (Expired $days days ago)"
                            }
                        }
                        else{$warrantyexp = "(unknown)"}
                        $vars = @{
                            model = $this.d($model, "(Model Unknown)")
                            comptype = $this.d($compType, "laptop")
                            computername = $this.d($RecSet.Fields.Item("COMPUTER_NAME").Value, "(Computer Name Unknown)")
                            servicetag = $this.d($RecSet.Fields.Item("SERVICE_TAG").Value, "(Unknown)")
                            owner = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown)")
                            ondomain = $this.d($ondomain, "(Unknown)")
                            lastproblem = $this.d($RecSet.Fields.Item("LAST_PROBLEM").Value, "(none)")
                            description = $this.d($RecSet.Fields.Item("DESCRIPTION").Value, "(none)")
                            lastservice = $lastservice
                            warrantyexpiration = $warrantyexp
                        }
                        $this.html = (Read-Html "computer-body-compact.html" $vars)
                    }
                    else{
                        $this.html = "multiple"
                    }
                    $RecSet.Close()
                }

                "getmodels"{
                    $query = "SELECT * FROM Tbl_WorkOrderModels ORDER BY MANUFACTURER, MODEL"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {
                            $compType = "Desktop"
                            $compTypeAbbr = "WK"
                            if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ 
                                $compType = "Laptop" 
                                $compTypeAbbr = "NB"
                            }    
                            $this.html = -join($this.html, '<option value="', $RecSet.Fields.Item("ID").Value, '" typeabbreviation="', $compTypeAbbr ,'">', $RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("MODEL").Value, " (", $compType, ')</option>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "getreasons"{
                    $query = "SELECT * FROM Tbl_WOReasonList ORDER BY [Reason]"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {
                            $this.html = -join($this.html, '<option value="', $RecSet.Fields.Item("ID").Value, '" header="', $RecSet.Fields.Item("IssueDescriptionHeader").Value.Replace('"', "&quot;"), '" shortdesc="', $RecSet.Fields.Item("ShortDescription").Value.Replace('"', "&quot;"))
                            $this.html = -join($this.html, '" goodexample="', $RecSet.Fields.Item("IssueDescriptionGoodEx").Value.Replace('"', "&quot;") ,'"')
                            $this.html = -join($this.html, 'reimage="',$RecSet.Fields.Item("ReimageRequired").Value , '">', $RecSet.Fields.Item("Reason").Value, '</option>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Reasons:" $this.html
                    }
                    $RecSet.Close()
                }

                Default {}
            }
        }
    }
    updateData($parameters, $data){
        Write-Host "updateData($parameters, $data)"
        if($parameters -is [hashtable]){
            foreach ($par in $parameters.GetEnumerator()) {
                switch ($par.key) {
                    "confirmadmin"{
                        $this.confirmAdmin($data)
                    }
                    "submitworkorder"{
                        $this.submitWorkOrder($data)
                    }
                }
            }
        }
    }

    confirmAdmin($data){
        $username = $this.s($data.item('username'))
        $password = $this.s($data.item('password'))
        $query = "SELECT * FROM Tbl_Users WHERE USERNAME = '$username'"
        try{
            if($password -and $username){
                $RecSet = $this.database.openRecordSet($query, 3, 1)
                if($RecSet.State -le 0){
                    $this.html = ""
                }
                else{    
                    $storedPass = $RecSet.Fields.Item("PASSWORD").Value
                    $salt = $storedPass.substring(64)
                    $saltedPw = -join($salt, $password)
                    $hash = -join($this.getStringHash($saltedPw, "SHA256"), $salt)
                    if($hash -eq $storedPass){
                        $this.html = "success"
                    }
                    else{
                        $this.html = ""
                    }
                }
            }
            else{
                $this.html = ""
            }
        }
        catch [Exception]{
            Write-Host $_.Exception.GetType().FullName "\n" $_.Exception.Message "\n" $query
            $this.html = "failed"
        }

    }
    submitWorkOrder($data){
        $query = ""
        try{
            #Write-Host ($data.keys | %{-join($_, ": ", $data.$_, "`n")})
            $userId = $data.userid
            $query = "SELECT * FROM (Tbl_Users INNER JOIN Tbl_Ranks ON Tbl_Users.RANK = Tbl_Ranks.ID) INNER JOIN Tbl_DirectorateCodes ON Tbl_DirectorateCodes.ID = Tbl_Users.DIRECTORATE WHERE Tbl_Users.ID = $userId"
            $RecSet = $this.database.openRecordSet($query, 3, 1)
            $name = -join($RecSet.Fields.Item("Tbl_Ranks.RANK").Value, " ", $RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("L_NAME").Value)
            $unit = $this.d($RecSet.Fields.Item("UNIT").Value, "(Unit Unknown)")
            $username = $this.d($RecSet.Fields.Item("USERNAME").Value, "(Username Unknown)")
            $workPhone = $this.formatPhone($this.d($RecSet.Fields.Item("WORK_PHONE").Value, "(none)"))
            $cellPhone = $this.formatPhone($this.d($RecSet.Fields.Item("CELL_PHONE").Value, "(none)"))
            $email = $this.d($RecSet.Fields.Item("EMAIL").Value, "(none)")
            $employment = $this.d($RecSet.Fields.Item("EMPLOYMENT").Value, "(Unknown Employment Status)")
            $directorate = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown Directorate)")
            $location = $this.d($RecSet.Fields.Item("LOCATION").Value, "(Unknown Location)")
            $isAdmin = "";
            if($RecSet.Fields.Item("IsAdmin").Value -eq 1){$isAdmin = "<strong>Administrator</strong>"}

           
            $adminUsername = $data.admin
            $query = "SELECT FIRST(ID) as AID FROM Tbl_Users WHERE USERNAME = '$adminUsername'"
            $RecSet = $this.database.openRecordSet($query, 3, 1)
            $adminId = $RecSet.Fields.Item("AID").value   
            $now = (Get-Date -Format G)
            $comments = $this.database.sanitizeString($data.comments)
            $query = "INSERT INTO Tbl_Workorders (USER_ID, RECEIVER, CREATED, STATUS, COMMENTS) VALUES ($userId, $adminId, '$now', 0, '$comments')"
            #Write-Host $query
            $this.database.execute($query)
            $query = "SELECT FIRST(ID) as WOID FROM Tbl_Workorders WHERE USER_ID = $userId AND RECEIVER = $adminId AND CREATED = #$now# AND STATUS = 0 AND COMMENTS = '$comments'"
            $RecSet = $this.database.openRecordSet($query, 3, 1)
            $woId = $RecSet.Fields.Item("WOID").value

            $woHeaderParams = @{}
            $woHeaderParams["woNumber"] = $woId
            $woHeaderParams["timestamp"] = $now
            $woHeaderParams["name"] = $name
            $woHeaderParams["isAdmin"] = $isAdmin
            $woHeaderParams["username"] = $username
            $woHeaderParams["unit"] = $unit
            $woHeaderParams["workPhone"] = $workPhone
            $woHeaderParams["cellPhone"] = $cellPhone
            $woHeaderParams["email"] = $email
            $woHeaderParams["employment"] = $employment
            $woHeaderParams["directorate"] = $directorate
            $woHeaderParams["location"] = $location
            $woHeaderParams["computerList"] = ""
            Write-Output "Header Params: $woHeaderParams" 

            $computers = @()
            foreach($item in $data.GetEnumerator()){
                Write-Output "Computer: $item"
                if($item.key -match "(computers)(\[\d+])(\[\w+])"){
                    $num = $Matches[2].Replace("[", "").Replace("]", "")
                    while($computers[$num] -eq $null -and $computers.length -lt $data.Count){
                        $computers += @{}
                    }
                    $attr = $Matches[3].Replace("[", "").Replace("]", "")
                    $computers[$num][$attr] = $item.value
                }
            }
            $compNumber = 0
            foreach($computer in $computers){
                $compNumber++
                Write-Output "Computer $compNumber"
                $woComputerParams = @{}
                $woComputerParams["timestamp"] = $now
                $woComputerParams["woNumber"] = $woId
                $woComputerParams["total"] = $computers.Count
                $woComputerParams["count"] = $compNumber
                $woComputerParams["name"] = $name
                $woComputerParams["username"] = $username
                $woComputerParams["isAdmin"] = $isAdmin
                $woComputerParams["unit"] = $unit
                $woComputerParams["workPhone"] = $workPhone
                $woComputerParams["cellPhone"] = $cellPhone
                $woComputerParams["email"] = $email
                $woComputerParams["employment"] = $employment
                $woComputerParams["directorate"] = $directorate
                $woComputerParams["location"] = $location
                $serviceTag = $computer.serviceTag
                $woComputerParams["servicetag"] = $serviceTag
                $reimage = $computer.reimage
                $reimageTF = if($reimage -eq "Yes"){$true}else{$false}
                $woComputerParams["reimage"] = $reimage
                $probDescription = $computer.comments
                $woComputerParams["problemdesc"] = $probDescription
                $reason = $computer.reason
                $woComputerParams["problem"] = $reason
                $query = "SELECT * FROM (Tbl_Computers INNER JOIN Tbl_ComputerModels ON Tbl_Computers.MODEL = Tbl_ComputerModels.ID) INNER JOIN Tbl_DirectorateCodes ON Tbl_DirectorateCodes.ID = Tbl_Computers.OWNER WHERE SERVICE_TAG = '$serviceTag'"
                $RecSet = $this.database.openRecordSet($query, 3, 1)
                $compId = $RecSet.Fields.Item("Tbl_Computers.ID").Value
                $model = $RecSet.Fields.Item("Tbl_ComputerModels.MODEL").Value
                $compName = $RecSet.Fields.Item("COMPUTER_NAME").Value
                $woComputerParams["computername"] = $compName
                $compDescription = $RecSet.Fields.Item("DESCRIPTION").Value
                $woComputerParams["description"] = $compDescription
                $lastProblem = $RecSet.Fields.Item("LAST_PROBLEM").Value
                $woComputerParams["lastproblem"] = $lastProblem
                $owner = $RecSet.Fields.Item("DirectorateDesc").Value
                $dirCode = $RecSet.Fields.Item("DirectorateCode").Value
                $woComputerParams["owner"] = -join($owner, " (", $dirCode, ")")
                $compType = "Desktop"
                if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ $compType = "Laptop" }
                $model = -join($RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("Tbl_ComputerModels.MODEL").Value, " ", $compType)
                $woComputerParams["model"] = $model
                $ondomain = "No"
                $woComputerParams["ondomain"] = "<i>Off Domain</i>"
                if($RecSet.Fields.Item("ON_DOMAIN").Value -eq 1){ $ondomain = "Yes" }
                if($RecSet.Fields.Item("ON_DOMAIN").Value -eq 1){ $ondomain = "On Domain" }
                $lastservice = $RecSet.Fields.Item("LAST_SERVICED").Value
                if($lastservice -as [datetime]){
                    $ts = New-TimeSpan -Start $lastservice -End (Get-Date)
                    $days = $ts.Days
                    $date = [DateTime]::parse($lastservice)
                    $lastservice =  get-date $date -Format "d MMM yyyy"
                    $lastservice = "$lastservice ($days days ago)"
                }
                $woComputerParams["lastservice"] = $lastservice
                $warrantyexp = $RecSet.Fields.Item("WARRANTY_EXPIRATION").Value
                if($warrantyexp -as [DateTime]){
                    $ts = New-TimeSpan -End $warrantyexp -Start (Get-Date)
                    $days = $ts.Days
                    $date = [DateTime]::parse($warrantyexp)
                    $warrantyexp =  get-date $date -Format "d MMM yyyy"
                    if($days -ge 0){
                        $warrantyexp = "$warrantyexp ($days days from now)"
                    }
                    else{
                        $days = [math]::abs($days)
                        $warrantyexp = "$warrantyexp (Expired $days days ago)"
                    }
                }


                $woComputerParams["warrantyexpiration"] = $warrantyexp
                $woHeaderParams["computerList"] += (Read-Html "print-wo-body.html" $woComputerParams)
                
                $query = "INSERT INTO Tbl_Comp_WO_Intersect (COMPUTER_ID, WORKORDER_ID, REIMAGE, REASON, DESCRIPTION) VALUES ($compId, $woId, $reimageTF, $reason, '$probDescription')"
                #Write-Host $query
                $this.database.execute($query)
            }

            $query = "SELECT FIRST(SET_VALUE) as PATH FROM Tbl_Settings WHERE SETTING = 'workorder_path'"
            $RecSet = $this.database.openRecordSet($query, 3, 1)
            $woPath = $RecSet.Fields.Item("PATH").value   
            $woPath = $woPath.Replace('$SERVERDIR', $Global:serverPath)


            $shortnow = get-date $now -Format "d-MMM-yyyy"
            $filename = "WO#$woId - $name ($shortnow)"
            $fullpath = [io.path]::combine("$woPath", "html", "$filename")
            $woHeaderParams["filename"] = $filename
            $woHeaderParams["woPath"] = $woPath
            $woHeaderParams["printcss"] = Get-Content ".\html\css\print.css"
            $workorderDocument = (Read-Html "print-wo-header.html" $woHeaderParams)
            $destFile = (-join($fullpath, ".pdf"))
            $workorderDocument | Out-File ($destFile.Replace(".pdf",".html"))
            ConvertFrom-HTMLtoPDF -Source $workorderDocument -Destination $destFile

            $this.html = -join($fullpath, ".html")
        }
        catch {
            Write-Host $_.Exception.GetType().FullName "`n" $_.Exception.Message "`n" $query
            $this.html = "failed"
        }
    }

    [String] getHtml(){
        return $this.html
    }
    cleanup(){
        $this.database.cleanup()
    }
    [string] d($str, $def){
        # Sets a string to a default if it is empty
        if([String]::IsNullOrEmpty($str)){
            $str = $def
        }
        return $str
    }
    [string] s($str){
        # Convenience wrapper
        return $this.database.sanitizeString($str)
    }
    [string] formatPhone($str){
        if($str -match '\d{4,11}'){
            switch($str.length){
                11{
                    $str = $str.insert(1," ")
                    $str = $str.insert(4,"-")
                    $str = $str.insert(8,"-")
                }
                10{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                9{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                8{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                7{
                    $str = $str.insert(3,"-")
                }
                6{
                    $str = $str.insert(3,"-")
                }
                5{
                    $str = $str.insert(3,"-")
                 }
                4{
                    $str = $str.insert(3,"-")
                }
            }
        }
        return $str
    }
    # https://gallery.technet.microsoft.com/scriptcenter/Get-StringHash-aa843f71
    # http://jongurgul.com/blog/get-stringhash-get-filehash/ 
    [string] getStringHash([String] $String, $HashName = "MD5") { 
        $StringBuilder = New-Object System.Text.StringBuilder 
        [System.Security.Cryptography.HashAlgorithm]::Create($HashName).ComputeHash([System.Text.Encoding]::UTF8.GetBytes($String))|%{ 
            [Void]$StringBuilder.Append($_.ToString("x2")) 
        }
        return $StringBuilder.ToString() 
    }
}