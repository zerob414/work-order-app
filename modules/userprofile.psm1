Using Module .\database.psm1

class UserProfile {
    $databasePath = ".\db\WorkOrderDB.accdb"
    $database = $null
    $html = ""

    UserProfile(){}
    UserProfile($databasePath){
        $this.databasePath = $databasePath
        $this.database = [Database]::new($databasePath)
    }
    UserProfile($databasePath, $parameters){
        $this.databasePath = $databasePath
        $this.database = [Database]::new($databasePath)
        foreach ($par in $parameters.GetEnumerator()) {
            switch ($par.key) {
                "lookup" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM (Tbl_Users INNER JOIN Tbl_Ranks ON Tbl_Users.RANK = Tbl_Ranks.ID) INNER JOIN Tbl_DirectorateCodes ON Tbl_DirectorateCodes.ID = Tbl_Users.DIRECTORATE WHERE USERNAME LIKE `"%$val%`" OR L_NAME LIKE '%$val%' OR F_NAME LIKE '%$val%'"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    Write-Host "Records Returned: " $RecSet.RecordCount
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    elseif($RecSet.RecordCount -eq 1){
                        $RecSet.MoveFirst()
                        $name = -join($RecSet.Fields.Item("Tbl_Ranks.RANK").Value, " ", $RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("L_NAME").Value)
                        $isAdmin = "";
                        if($RecSet.Fields.Item("IsAdmin").Value -eq 1){$isAdmin = "<p><i class='fas fa-exclamation-triangle'></i><strong>Administrator</strong></p>"}
                        $created = $RecSet.Fields.Item("CREATED").Value
                        if($created -as [datetime]){
                            $ts = New-TimeSpan -Start $created -End (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($created)
                            $created =  get-date $date -Format "d MMM yyyy"
                            $created = "$created ($days days ago)"
                        }
                        else{$created = "(unknown)"}
                        $lastUsed = $RecSet.Fields.Item("LAST_USED").Value
                        if($lastUsed -as [DateTime]){
                            $ts = New-TimeSpan -Start $lastUsed -End (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($lastUsed)
                            $lastUsed =  get-date $date -Format "d MMM yyyy"
                            $lastUsed = "$lastUsed ($days days ago)"
                        }
                        else{$lastUsed = "(never)"}
                        $vars = @{
                            name = $this.d($name, "(Unnamed)")
                            unit = $this.d($RecSet.Fields.Item("UNIT").Value, "(Unit Unknown)")
                            username = $this.d($RecSet.Fields.Item("USERNAME").Value, "(Username Unknown)")
                            workPhone = $this.formatPhone($this.d($RecSet.Fields.Item("WORK_PHONE").Value, "(none)"))
                            cellPhone = $this.formatPhone($this.d($RecSet.Fields.Item("CELL_PHONE").Value, "(none)"))
                            email = $this.d($RecSet.Fields.Item("EMAIL").Value, "(none)")
                            employment = $this.d($RecSet.Fields.Item("EMPLOYMENT").Value, "(Unknown Employment Status)")
                            directorate = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown Directorate)")
                            location = $this.d($RecSet.Fields.Item("LOCATION").Value, "(Unknown Location)")
                            created = $created
                            lastUsed = $lastUsed
                            isAdmin = $isAdmin
                        }
                        $this.html = (Read-Html "profile-body.html" $vars)
                    }
                    else{
                        $RecSet.MoveFirst()
                        $this.html = "<div class='list'>"
                        do{
                            $username = $RecSet.Fields.Item("USERNAME").Value
                            $name = -join($RecSet.Fields.Item("Tbl_Ranks.RANK").Value, " ", $RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("L_NAME").Value)
                            $employment = $this.d($RecSet.Fields.Item("EMPLOYMENT").Value, "(Unknown Employment Status)")
                            $directorate = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown Directorate)")
                            $location = $this.d($RecSet.Fields.Item("LOCATION").Value, "(Unknown Location)")
                            $this.html += "<div class='list-item'>"
                            $this.html += " <a href=`"javascript:lookupUser('$username')`"><i class='fas fa-user'></i>"
                            $this.html += "  $name ($username) $employment at $directorate in $location"
                            $this.html += " </a></div>"
                            $RecSet.MoveNext()
                        }while(-not $RecSet.EOF)
                        $this.html += "</div>"
                    }
                    $RecSet.Close()
                }
                "lookupforedit" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM Tbl_Users WHERE USERNAME = `"$val`""
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $vars = @{
                            firstname = $RecSet.Fields.Item("F_NAME").Value
                            lastname = $RecSet.Fields.Item("L_NAME").Value
                            rank = $RecSet.Fields.Item("RANK").Value
                            unit = $RecSet.Fields.Item("UNIT").Value
                            username = $RecSet.Fields.Item("USERNAME").Value
                            workPhone = $this.formatPhone($RecSet.Fields.Item("WORK_PHONE").Value)
                            cellPhone = $this.formatPhone($RecSet.Fields.Item("CELL_PHONE").Value)
                            email = $RecSet.Fields.Item("EMAIL").Value
                            employment = $RecSet.Fields.Item("EMPLOYMENT").Value
                            directorate = $RecSet.Fields.Item("DIRECTORATE").Value
                            location = $RecSet.Fields.Item("LOCATION").Value
                            created = $RecSet.Fields.Item("CREATED").Value
                            lastUsed = $RecSet.Fields.Item("LAST_USED").Value
                            isAdmin = $RecSet.Fields.Item("IsAdmin").Value
                        }
                        $this.html = ($vars | convertTo-Json)
                    }
                    $RecSet.Close()
                }
                "delete"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        $query = "DELETE FROM Tbl_Users WHERE USERNAME = '$val'"
                        $this.database.execute($query)
                        # Check if delete was successful
                        $query = "SELECT * FROM Tbl_Users WHERE USERNAME = `"$val`""
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.EOF -OR $RecSet.State -eq 0){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }
                "getranks"{
                    $query = "SELECT * FROM Tbl_Ranks ORDER BY [ORDER]"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {
                            $this.html = -join($this.html, '<option value="', $RecSet.Fields.Item("ID").Value, '">', $RecSet.Fields.Item("RANK").Value, '</option>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "getdirectorates"{
                    $query = "SELECT * FROM Tbl_DirectorateCodes ORDER BY [Sort Order]"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {
                            $this.html = -join($this.html, '<option value="', $RecSet.Fields.Item("ID").Value, '">', $RecSet.Fields.Item("DirectorateDesc").Value, '</option>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }

                Default {}
            }
        }
    }
    updateUser($data){
        $username = $this.s($data.item('username'))
        $query = "SELECT * FROM Tbl_Users WHERE USERNAME = `"$username`""
        $RecSet = $this.database.openRecordSet($query, 3, 1)
        if($RecSet.State -gt 0){ # Make sure the record set opened
            if($RecSet.EOF){  # If 0 records are returned, we'll insert
                $query = "INSERT INTO Tbl_Users (USERNAME, F_NAME, L_NAME, UNIT, RANK, CREATED, WORK_PHONE, CELL_PHONE, EMAIL, LOCATION, DIRECTORATE, EMPLOYMENT) VALUES ("
                $query = -join($query, "'", $this.s($data.item('username')), "', '", $this.s($data.item('firstname')), "', '", $this.s($data.item('lastname')), "', '", $this.s($data.item('unit')), "', ", $this.s($data.item('rank')), ", '")
                $query = -join($query, (Get-Date -Format g), "', '", $this.s($data.item('workPhone')), "', '", $this.s($data.item('cellPhone')), "', '", $this.s($data.item('email')), "', '", $this.s($data.item('location')), "', '")
                $query = -join($query, $this.s($data.item('directorate')), "', '", $this.s($data.item('employment')), "')")
                Write-Host "query = $query"
                $this.database.execute($query)
            }
            else{  #if a record is returned, we'll update
                $query = -join("UPDATE Tbl_Users SET F_NAME='", $this.s($data.item('firstname')),"', L_NAME='", $this.s($data.item('lastname')), "', UNIT='",  $this.s($data.item('unit')), "', RANK=", $this.s($data.item('rank')))
                $query = -join($query, ", WORK_PHONE='", $this.s($data.item('workPhone')), "', CELL_PHONE='", $this.s($data.item('cellPhone')), "', EMAIL='", $this.s($data.item('email')), "', LOCATION='", $this.s($data.item('location')))
                $query = -join($query, "', DIRECTORATE='", $this.s($data.item('directorate')), "', EMPLOYMENT='", $this.s($data.item('employment')), "' WHERE USERNAME='$username'") 
                Write-Host "query = $query"
                $this.database.execute($query)
            }
        }
    }
    [String] getHtml(){
        return $this.html
    }
    cleanup(){
        $this.database.cleanup()
    }
    [string] d($str, $def){
        # Sets a string to a default if it is empty
        if([String]::IsNullOrEmpty($str)){
            $str = $def
        }
        return $str
    }
    [string] s($str){
        # Convenience wrapper
        return $this.database.sanitizeString($str)
    }
    [string] formatPhone($str){
        if($str -match '\d{4,11}'){
            switch($str.length){
                11{
                    $str = $str.insert(1," ")
                    $str = $str.insert(4,"-")
                    $str = $str.insert(8,"-")
                }
                10{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                9{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                8{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                7{
                    $str = $str.insert(3,"-")
                }
                6{
                    $str = $str.insert(3,"-")
                }
                5{
                    $str = $str.insert(3,"-")
                 }
                4{
                    $str = $str.insert(3,"-")
                }
            }
        }
        return $str
    }
}