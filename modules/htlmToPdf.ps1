$PrinterName = "Microsoft Print to PDF"
$oldPrinter = getDefaultPrinter

function setDefaultPrinter($PrinterName){
    $Printers = Get-WmiObject -Class Win32_Printer
    Try{ 
        Write-Verbose "Get the specified printer info." 
        $Printer = $Printers | Where{$_.Name -eq "$PrinterName"} 
    
        If($Printer){ 
            Write-Verbose "Setting the default printer." 
            $Printer.SetDefaultPrinter() | Out-Null 
    
            Write-Host "Successfully set the default printer." 
        } 
        Else{ 
            Write-Warning "Cannot find the specified printer." 
        } 
    } 
    Catch{ 
        $ErrorMsg = $_.Exception.Message 
        Write-Host $ErrorMsg -BackgroundColor Red 
    } 
}

function getDefaultPrinter{
    $Printers = Get-WmiObject -Class Win32_Printer
    try{
        return $Printers | Where{$_.Default -eq $true}
    }
    Catch{ 
        $ErrorMsg = $_.Exception.Message 
        Write-Host $ErrorMsg -BackgroundColor Red 
    }
}

setDefaultPrinter($PrinterName)

$ie = new-object -com "InternetExplorer.Application"
$ie.Navigate("file:///C:/Users/1471350750.mil/Documents/Help%20Desk%20Work%20Order%20Web%20App/workorders/html/WO#76%20-%20SGT%20Zachary%20Hunter%20(6-Dec-2018).html")
while ( $ie.busy ) { Start-Sleep -second 3 }
$ie.visible = $true
$ie.ExecWB(6,2)
Start-Sleep -second 3
while ( $ie.busy ) { Start-Sleep -second 3 }
$ie.quit()

setDefaultPrinter($oldPrinter.Name)