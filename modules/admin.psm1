Using Module .\database.psm1

class Admin {
    $databasePath = ".\db\WorkOrderDB.accdb"
    $database = $null
    $html = ""

    Admin(){}
    Admin($databasePath){
        $this.databasePath = $databasePath
        $this.database = [Database]::new($databasePath)
    }
    Admin($databasePath, $parameters){
        $this.databasePath = $databasePath
        $this.database = [Database]::new($databasePath)
        foreach ($par in $parameters.GetEnumerator()) {
            switch ($par.key) {
                "lookup" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM (Tbl_Computers INNER JOIN Tbl_ComputerModels ON Tbl_Computers.MODEL = Tbl_ComputerModels.ID) INNER JOIN Tbl_DirectorateCodes ON Tbl_DirectorateCodes.ID = Tbl_Computers.OWNER WHERE SERVICE_TAG LIKE '%$val%' OR COMPUTER_NAME LIKE '%$val%'"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    Write-Host "Records Returned: " $RecSet.RecordCount
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    elseif($RecSet.RecordCount -eq 1){
                        $RecSet.MoveFirst()
                        $compType = "Desktop"
                        if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ $compType = "Laptop" }
                        $model = -join($RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("Tbl_ComputerModels.MODEL").Value, " ", $compType)
                        $ondomain = "No"
                        if($RecSet.Fields.Item("ON_DOMAIN").Value -eq 1){ $ondomain = "Yes" }
                        $lastservice = $RecSet.Fields.Item("LAST_SERVICED").Value
                        if($lastservice -as [datetime]){
                            $ts = New-TimeSpan -Start $lastservice -End (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($lastservice)
                            $lastservice =  get-date $date -Format "d MMM yyyy"
                            $lastservice = "$lastservice ($days days ago)"
                        }
                        else{$lastservice = "(unknown)"}
                        $warrantyexp = $RecSet.Fields.Item("WARRANTY_EXPIRATION").Value
                        if($warrantyexp -as [DateTime]){
                            $ts = New-TimeSpan -End $warrantyexp -Start (Get-Date)
                            $days = $ts.Days
                            $date = [DateTime]::parse($warrantyexp)
                            $warrantyexp =  get-date $date -Format "d MMM yyyy"
                            if($days -ge 0){
                                $warrantyexp = "$warrantyexp ($days days from now)"
                            }
                            else{
                                $days = [math]::abs($days)
                                $warrantyexp = "$warrantyexp (Expired $days days ago)"
                            }
                        }
                        else{$warrantyexp = "(unknown)"}
                        $vars = @{
                            model = $this.d($model, "(Model Unknown)")
                            comptype = $this.d($compType, "laptop")
                            computername = $this.d($RecSet.Fields.Item("COMPUTER_NAME").Value, "(Computer Name Unknown)")
                            servicetag = $this.d($RecSet.Fields.Item("SERVICE_TAG").Value, "(Unknown)")
                            owner = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown)")
                            ondomain = $this.d($ondomain, "(Unknown)")
                            lastproblem = $this.d($RecSet.Fields.Item("LAST_PROBLEM").Value, "(none)")
                            description = $this.d($RecSet.Fields.Item("DESCRIPTION").Value, "(none)")
                            lastservice = $lastservice
                            warrantyexpiration = $warrantyexp
                        }
                        $this.html = (Read-Html "computer-body.html" $vars)
                    }
                    else{
                        $RecSet.MoveFirst()
                        $this.html = "<div class='list'>"
                        do{
                            $computername = $RecSet.Fields.Item("COMPUTER_NAME").Value
                            $compType = "Desktop"
                            if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ $compType = "Laptop" }    
                            $model = -join($RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("Tbl_ComputerModels.MODEL").Value, " ", $compType)
                            $owner = $this.d($RecSet.Fields.Item("DirectorateDesc").Value, "(Unknown Owner)")
                            $servicetag = $this.d($RecSet.Fields.Item("SERVICE_TAG").Value, "Unknown Service Tag")
                            $this.html += "<div class='list-item'>"
                            $this.html += " <a href=`"javascript:lookupComputer('$servicetag')`"><i class='fas fa-laptop'></i>"
                            $this.html += "  $computername ($servicetag) $model owned by $owner"
                            $this.html += " </a></div>"
                            $RecSet.MoveNext()
                        }while(-not $RecSet.EOF)
                        $this.html += "</div>"
                    }
                    $RecSet.Close()
                }
                "getpostforedit" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM Tbl_NEWS WHERE ID = $val"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $vars = @{
                            id = $RecSet.Fields.Item("ID").Value
                            title = $RecSet.Fields.Item("TITLE").Value.Replace('""', '"')
                            body = $RecSet.Fields.Item("BODY").Value.Replace('""', '"')
                            author = $RecSet.Fields.Item("AUTHOR").Value
                            timestamp = $RecSet.Fields.Item("TIMESTAMP").Value
                            editor = $RecSet.Fields.Item("EDITOR")
                            editedtimestamp = $RecSet.Fields.Item("EDITED_TIMESTAMP").Value
                        }
                        $this.html = ($vars | convertTo-Json)
                    }
                    $RecSet.Close()
                }
                "deletepost"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        $query = "DELETE FROM Tbl_News WHERE ID = $val"
                        $this.database.execute($query)
                        # Check if delete was successful
                        $query = "SELECT * FROM Tbl_News WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.EOF -OR $RecSet.State -eq 0){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }
                "getmodels"{
                    $query = "SELECT * FROM Tbl_ComputerModels ORDER BY MANUFACTURER, MODEL"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {
                            $compType = "Desktop"
                            $compTypeAbbr = "WK"
                            if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ 
                                $compType = "Laptop" 
                                $compTypeAbbr = "NB"
                            }    
                            $this.html = -join($this.html, '<option value="', $RecSet.Fields.Item("ID").Value, '" typeabbreviation="', $compTypeAbbr ,'">', $RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("MODEL").Value, " (", $compType, ')</option>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "getbanner"{
                    $query = "SELECT * FROM Tbl_Settings WHERE CATEGORY='banner'"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        $banner = @{}
                        do {
                            $banner.add($RecSet.Fields.Item("SETTING").Value, $RecSet.Fields.Item("SET_VALUE").Value.Replace('""', '"'))
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                        $this.html = $banner | convertTo-Json
                    }
                    $RecSet.Close()
                }
                "getpostcount"{
                    $query = "SELECT Count(*) AS PostCount FROM Tbl_News"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    $this.html = $RecSet.Fields.Item("PostCount").Value
                    $RecSet.Close()
                }
                "getposts"{
                    $offset = $par.value
                    $limit = 20
                    $query = "SELECT * FROM Tbl_News ORDER BY [timestamp] DESC"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    $RecSet.MoveFirst()
                    #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                    # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                    $repetitions = 0 
                    $posts = "" #New-Object System.Collections.ArrayList
                    do {
                        if($repetitions -ge $offset){
                            $post = @{}
                            $post.add("id", $RecSet.Fields.Item("ID").Value)
                            $post.add("title", $RecSet.Fields.Item("TITLE").Value.Replace('""', '"'))
                            $post.add("body", $RecSet.Fields.Item("BODY").Value.Replace('""', '"'))
                            $post.add("author", $RecSet.Fields.Item("AUTHOR").Value)
                            $post.add("timestamp", $RecSet.Fields.Item("TIMESTAMP").Value)
                            $posts += (Read-Html "post-body.html" $post)
                            #$posts.add($post)
                        }
                        $RecSet.MoveNext()
                        $repetitions++  #Do not remove this line! Infinite loops will ensue!
                    } until($RecSet.EOF -eq $True -OR $repetitions -gt ($offset + $limit)  -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                    $this.html = $posts #| convertTo-Json

                }
                "getcomputermodels"{
                    $query = "SELECT * FROM Tbl_ComputerModels ORDER BY MANUFACTURER, MODEL"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {
                            $compType = "Desktop"
                            $compTypeAbbr = "WK"
                            if($RecSet.Fields.Item("IS_LAPTOP").Value -eq 1){ 
                                $compType = "Laptop" 
                                $compTypeAbbr = "NB"
                            }    
                            $this.html = -join($this.html, '<tr id="model', $RecSet.Fields.Item("ID").Value, '">')
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("MANUFACTURER").Value, "</td>")
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("MODEL").Value, "</td><td>", $compType, '</td>')
                            $this.html = -join($this.html, '<td class="center-text button-tray"><a title="Edit" href="javascript:editComputerModel(', $RecSet.Fields.Item("ID").Value)
                            $this.html = -join($this.html, ')"<i class="fas fa-pencil-alt"></i></a><a title="Delete" href="javascript:deleteComputerModel(') 
                            $this.html = -join($this.html, $RecSet.Fields.Item("ID").Value, ", '", $RecSet.Fields.Item("MANUFACTURER").Value, " ", $RecSet.Fields.Item("MODEL").Value, " ", $compType ,"')`"", '><i class="fas fa-trash-alt"></i></a></td></tr>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "getcomputermodelforedit" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM Tbl_ComputerModels WHERE ID = $val"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $vars = @{
                            id = $RecSet.Fields.Item("ID").Value
                            manufacturer = $RecSet.Fields.Item("MANUFACTURER").Value.Replace('""', '"')
                            model = $RecSet.Fields.Item("MODEL").Value.Replace('""', '"')
                            islaptop = $RecSet.Fields.Item("IS_LAPTOP").Value
                        }
                        $this.html = ($vars | convertTo-Json)
                    }
                    $RecSet.Close()
                }
                "deletecomputermodel"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        $query = "DELETE FROM Tbl_ComputerModels WHERE ID = $val"
                        $this.database.execute($query)
                        # Check if delete was successful
                        $query = "SELECT * FROM Tbl_ComputerModels WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.EOF -OR $RecSet.State -eq 0){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }
                "getreasons"{
                    $query = "SELECT * FROM Tbl_WOReasonList ORDER BY Reason"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {
                            $reimage = "No"
                            if($RecSet.Fields.Item("ReimageRequired").Value -eq 1){ 
                                $reimage = "Yes" 
                            }    
                            $this.html = -join($this.html, '<tr id="reason', $RecSet.Fields.Item("ID").Value, '">')
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("Reason").Value, "</td>")
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("ShortDescription").Value, "</td><td>", $RecSet.Fields.Item("IssueDescriptionHeader").Value, '</td>')
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("IssueDescriptionGoodEx").Value, "</td><td>", $RecSet.Fields.Item("IssueDescriptionBadEx").Value, '</td>')
                            $this.html = -join($this.html, "<td>", $reimage, "</td>")
                            $this.html = -join($this.html, '<td class="center-text button-tray">')
                            $this.html = -join($this.html,'<a title="Edit" href="javascript:editReason(', $RecSet.Fields.Item("ID").Value)
                            $this.html = -join($this.html, ')"<i class="fas fa-pencil-alt"></i></a><a title="Delete" href="javascript:deleteReason(') 
                            $this.html = -join($this.html, $RecSet.Fields.Item("ID").Value, ", '", $RecSet.Fields.Item("Reason").Value, "')`"", '><i class="fas fa-trash-alt"></i></a></td></tr>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "getreasonforedit" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM Tbl_WOReasonList WHERE ID = $val"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $vars = @{
                            id = $RecSet.Fields.Item("ID").Value
                            reason = $RecSet.Fields.Item("Reason").Value.Replace('""', '"')
                            shortdesc = $RecSet.Fields.Item("ShortDescription").Value.Replace('""', '"')
                            longdesc = $RecSet.Fields.Item("IssueDescriptionHeader").Value.Replace('""', '"')
                            goodexample = $RecSet.Fields.Item("IssueDescriptionGoodEx").Value.Replace('""', '"')
                            badexample = $RecSet.Fields.Item("IssueDescriptionBadEx").Value.Replace('""', '"')
                            reimagerec = $RecSet.Fields.Item("ReimageRequired").Value
                        }
                        $this.html = ($vars | convertTo-Json)
                    }
                    $RecSet.Close()
                }
                "deletereason"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        $query = "DELETE FROM Tbl_WOReasonList WHERE ID = $val"
                        $this.database.execute($query)
                        # Check if delete was successful
                        $query = "SELECT * FROM Tbl_WOReasonList WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.EOF -OR $RecSet.State -eq 0){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }
                "getdirectorates"{
                    $query = "SELECT * FROM Tbl_DirectorateCodes ORDER BY DirectorateDesc,DirectorateCode"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {   
                            $this.html = -join($this.html, '<tr id="directorate', $RecSet.Fields.Item("ID").Value, '">')
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("DirectorateDesc").Value, "</td>")
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("DirectorateCode").Value, "</td>")
                            $this.html = -join($this.html, '<td class="center-text button-tray"><a title="Edit" href="javascript:editDirectorate(', $RecSet.Fields.Item("ID").Value)
                            $this.html = -join($this.html, ')"<i class="fas fa-pencil-alt"></i></a><a title="Delete" href="javascript:deleteDirectorate(') 
                            $this.html = -join($this.html, $RecSet.Fields.Item("ID").Value, ", '", $RecSet.Fields.Item("DirectorateDesc").Value, "')`"", '><i class="fas fa-trash-alt"></i></a></td></tr>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "getdirectorateforedit" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM Tbl_DirectorateCodes WHERE ID = $val"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $vars = @{
                            id = $RecSet.Fields.Item("ID").Value
                            directorate = $RecSet.Fields.Item("DirectorateDesc").Value.Replace('""', '"')
                            code = $RecSet.Fields.Item("DirectorateCode").Value.Replace('""', '"')
                            sortorder = $RecSet.Fields.Item("SortOrder").Value
                        }
                        $this.html = ($vars | convertTo-Json)
                    }
                    $RecSet.Close()
                }
                "deletedirectorate"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        $query = "DELETE FROM Tbl_DirectorateCodes WHERE ID = $val"
                        $this.database.execute($query)
                        # Check if delete was successful
                        $query = "SELECT * FROM Tbl_DirectorateCodes WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.EOF -OR $RecSet.State -eq 0){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }
                "getranks"{
                    $query = "SELECT * FROM Tbl_Ranks ORDER BY BRANCH,[ORDER],RANK"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do {   
                            $this.html = -join($this.html, '<tr id="rank', $RecSet.Fields.Item("ID").Value, '">')
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("RANK").Value, "</td>")
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("BRANCH").Value, "</td>")
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("ORDER").Value, "</td>")
                            $this.html = -join($this.html, '<td class="center-text button-tray">')
                            $this.html = -join($this.html, '<a title="Move Up" href="javascript:moveRankUp(', $RecSet.Fields.Item("ID").Value, ", '", $RecSet.Fields.Item("RANK").Value, "'", ')"<i class="fas fa-arrow-up"></i></a>')
                            $this.html = -join($this.html, '<a title="Move Down" href="javascript:moveRankDown(', $RecSet.Fields.Item("ID").Value, ", '", $RecSet.Fields.Item("RANK").Value, "'", ')"<i class="fas fa-arrow-down"></i></a>')
                            $this.html = -join($this.html, '<a title="Edit" href="javascript:editRank(', $RecSet.Fields.Item("ID").Value, ')"<i class="fas fa-pencil-alt"></i></a>')
                            $this.html = -join($this.html, '<a title="Delete" href="javascript:deleteRank(') 
                            $this.html = -join($this.html, $RecSet.Fields.Item("ID").Value, ", '", $RecSet.Fields.Item("RANK").Value, "')`"", '><i class="fas fa-trash-alt"></i></a></td></tr>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "moverankup"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        # Get order value of selected rank
                        $query = "SELECT ORDER,BRANCH FROM Tbl_Ranks WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        $ord = $RecSet.Fields.Item("ORDER").Value
                        $branch = $RecSet.Fields.Item("BRANCH").Value
                        $ordLess = $ord - 1

                        # Make sure it's greater than 1
                        if($ord -gt 1){ 
                            # Get ID of rank above selected rank
                            $query = "SELECT ID FROM Tbl_Ranks WHERE BRANCH = '$BRANCH' AND [ORDER] = $ordLess"
                            $RecSet = $this.database.openRecordSet($query, 3, 1)
                            $otherId = $RecSet.Fields.Item("ID").Value
                            Write-Host $query

                            # Set order value of selected rank one less than current value
                            $query = "UPDATE Tbl_Ranks SET [ORDER] = $ordLess WHERE ID = $val"
                            $this.database.execute($query)
                            Write-Host $query

                            # Set order value of other rank to one more than current value
                            $query = "UPDATE Tbl_Ranks SET [ORDER] = $ord WHERE ID = $otherId"
                            $this.database.execute($query)
                            Write-Host $query
                        }

                        $query = "SELECT ORDER FROM Tbl_Ranks WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.Fields.Item("ORDER").Value -eq $ordLess){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }
                "moverankdown"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        # Get order value of selected rank
                        $query = "SELECT ORDER,BRANCH FROM Tbl_Ranks WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        $ord = $RecSet.Fields.Item("ORDER").Value
                        $branch = $RecSet.Fields.Item("BRANCH").Value
                        $ordMore = $ord + 1

                        $query = "SELECT MAX([ORDER]) AS BMAX FROM Tbl_Ranks WHERE BRANCH = '$BRANCH'"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        $max = $RecSet.Fields.Item("BMAX").Value

                        # Make sure it's less than the max
                        if($ord -lt $max){ 
                            # Get ID of rank below selected rank
                            $query = "SELECT ID FROM Tbl_Ranks WHERE BRANCH = '$BRANCH' AND [ORDER] = $ordMore"
                            $RecSet = $this.database.openRecordSet($query, 3, 1)
                            $otherId = $RecSet.Fields.Item("ID").Value
                            Write-Host $query

                            # Set order value of selected rank one more than current value
                            $query = "UPDATE Tbl_Ranks SET [ORDER] = $ordMore WHERE ID = $val"
                            $this.database.execute($query)
                            Write-Host $query

                            # Set order value of other rank to one less than current value
                            $query = "UPDATE Tbl_Ranks SET [ORDER] = $ord WHERE ID = $otherId"
                            $this.database.execute($query)
                            Write-Host $query
                        }

                        $query = "SELECT ORDER FROM Tbl_Ranks WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.Fields.Item("ORDER").Value -eq $ordMore){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }

                "getrankforedit" {
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM Tbl_Ranks WHERE ID = $val"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $vars = @{
                            id = $RecSet.Fields.Item("ID").Value
                            rank = $RecSet.Fields.Item("RANK").Value.Replace('""', '"')
                            branch = $RecSet.Fields.Item("BRANCH").Value.Replace('""', '"')
                            order = $RecSet.Fields.Item("ORDER").Value
                        }
                        $this.html = ($vars | convertTo-Json)
                    }
                    $RecSet.Close()
                }
                "deleterank"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        $query = "DELETE FROM Tbl_Ranks WHERE ID = $val"
                        $this.database.execute($query)
                        # Check if delete was successful
                        $query = "SELECT * FROM Tbl_Ranks WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.EOF -OR $RecSet.State -eq 0){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }
                "getadmins"{
                    $query = "SELECT * FROM (Tbl_Users INNER JOIN Tbl_Ranks ON Tbl_Users.RANK = Tbl_Ranks.ID) INNER JOIN Tbl_DirectorateCodes ON Tbl_DirectorateCodes.ID = Tbl_Users.DIRECTORATE WHERE IsAdmin = true ORDER BY USERNAME"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do { 
                            $name = -join($RecSet.Fields.Item("Tbl_Ranks.RANK").Value, " ", $RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("MI").Value, " ", $RecSet.Fields.Item("L_NAME").Value)
                            $id = $RecSet.Fields.Item("Tbl_Users.ID").Value
                            $hasPassword = "No"
                            if($RecSet.Fields.Item("PASSWORD").Value.length -ge 126){
                                $hasPassword = "Yes"
                            }
                            $this.html = -join($this.html, '<tr id="admin', $id, '">')
                            $this.html = -join($this.html, "<td id='name$id'>", $name, "</td>")
                            $this.html = -join($this.html, "<td>", $this.formatPhone($RecSet.Fields.Item("WORK_PHONE").Value), "</td>")
                            $this.html = -join($this.html, "<td>", $this.formatPhone($RecSet.Fields.Item("CELL_PHONE").Value), "</td>")
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("EMAIL").Value, "</td>")
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("LOCATION").Value, "</td>")
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("DirectorateDesc").Value, "</td>")
                            $this.html = -join($this.html, "<td>", $RecSet.Fields.Item("EMPLOYMENT").Value, "</td>")
                            $this.html = -join($this.html, "<td>$hasPassword</td>")
                            $this.html = -join($this.html, "<td class='center-text button-tray'><a title='Change Password' href='javascript:changeAdminPassword($id)'><i class='fas fa-key'></i>") 
                            $this.html = -join($this.html, '<a title="Remove Admin" href="javascript:deleteAdmin(') 
                            $this.html = -join($this.html, "$id)`"", '><i class="fas fa-trash-alt"></i></a></td></tr>')
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "deleteadmin"{
                    $val = $this.database.sanitizeString($par.value)
                    if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                        $query = "UPDATE Tbl_Users SET IsAdmin = false, PASSWORD = '' WHERE ID = $val"
                        $this.database.execute($query)
                        # Check if delete was successful
                        $query = "SELECT IsAdmin FROM Tbl_Users WHERE ID = $val"
                        $RecSet = $this.database.openRecordSet($query, 3, 1)
                        if($RecSet.Fields.Item("IsAdmin").value -eq 0){
                            $this.html = "success"
                        }
                        else{
                            $this.html = "fail";
                        }
                    }
                    else{
                        $this.html = "fail";
                    }
                }
                "getusers"{
                    $val = $this.database.sanitizeString($par.value)
                    $query = "SELECT * FROM (Tbl_Users INNER JOIN Tbl_Ranks ON Tbl_Users.RANK = Tbl_Ranks.ID) INNER JOIN Tbl_DirectorateCodes ON Tbl_DirectorateCodes.ID = Tbl_Users.DIRECTORATE WHERE USERNAME LIKE `"%$val%`" OR L_NAME LIKE '%$val%' OR F_NAME LIKE '%$val%'"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = "<p>No matches found.</p>"
                    }
                    else{
                        $RecSet.MoveFirst()
                        #I discovered if an error occurs opening the record set above, EOF will never be set to true, so the do loop below will repeat infinitely
                        # displaying endless errors. The repetitions variable is to ensure an infinite loop won't happen.
                        $repetitions = 0 
                        do { 
                            $name = -join($RecSet.Fields.Item("Tbl_Ranks.RANK").Value, " ", $RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("MI").Value, " ", $RecSet.Fields.Item("L_NAME").Value)
                            $id = $RecSet.Fields.Item("Tbl_Users.ID").Value
                            $this.html = -join($this.html, "<div id='user$id' class='list-item'>")
                            $this.html = -join($this.html, "<a href='javascript:makeAdmin($id, `"$name`")' style='text-decoration: none;'")
                            $this.html = -join($this.html, " title='", $RecSet.Fields.Item("EMAIL").Value, "'>")
                            $this.html = -join($this.html, $name, " <small>(")
                            $this.html = -join($this.html, $RecSet.Fields.Item("DirectorateDesc").Value, ", ")
                            $this.html = -join($this.html, $RecSet.Fields.Item("LOCATION").Value, ", ")
                            $this.html = -join($this.html, $RecSet.Fields.Item("EMPLOYMENT").Value, ")</small>")
                            $this.html = -join($this.html, '</a></div>') 
                            $RecSet.MoveNext()
                            $repetitions++  #Do not remove this line! Infinite loops will ensue!
                        } until($RecSet.EOF -eq $True -OR $repetitions -gt $RecSet.RecordCount) #RecordCount returns -1 if there is an error and 0 if there are no records.
                        #Write-Host "Ranks:" $this.html
                    }
                    $RecSet.Close()
                }
                "getstorageloc"{
                    $query = "SELECT FIRST(SET_VALUE) as PATH FROM Tbl_Settings WHERE SETTING='workorder_path'"
                    $RecSet = $this.database.openRecordSet($query, 3, 1)
                    if($RecSet.EOF -OR $RecSet.State -eq 0){
                        $this.html = ""
                    }
                    else{
                        $RecSet.MoveFirst()
                        $this.html = $RecSet.Fields.Item("PATH").Value.Replace('""', '"')
                    }
                    $RecSet.Close()

                }

                Default {}
            }
        }
    }
    updateData($parameters, $data){
        Write-Host "updateData($parameters, $data)"
        if($parameters -is [hashtable]){
            foreach ($par in $parameters.GetEnumerator()) {
                switch ($par.key) {
                    "submitbanner"{
                        $this.submitBanner($data)
                    }
                    "submitpost"{
                        $this.submitPost($data)
                    }
                    "submitmodel"{
                        $this.submitModel($data)
                    }
                    "submitreason"{
                        $this.submitReason($data)
                    }
                    "submitdirectorate"{
                        $this.submitDirectorate($data)
                    }
                    "submitrank"{
                        $this.submitRank($data)
                    }
                    "submitadmin"{
                        $this.submitAdmin($data)
                    }
                    "submitpassword"{
                        $this.submitPassword($data)
                    }
                    "login"{
                        $this.login($data)
                    }
                    "submitlocation"{
                        $this.submitLocation($data)
                    }
                    "testlocation"{
                        $this.testLocation($data)
                    }
                }
            }
        }
    }
    submitBanner($data){
        $query = "SELECT * FROM Tbl_Settings WHERE CATEGORY = `"banner`""
        $RecSet = $this.database.openRecordSet($query, 3, 1)
        if($RecSet.State -gt 0){ # Make sure the record set opened
            try{
                if($RecSet.EOF){  # If 0 records are returned, we'll insert
                    $query = -join("INSERT INTO Tbl_Settings (SETTING, SET_VALUE, CATEGORY) VALUES ('showbanner', '", $this.s($data.item('showbanner')), "', 'banner')")
                    $this.database.execute($query)
                    $query = -join("INSERT INTO Tbl_Settings (SETTING, SET_VALUE, CATEGORY) VALUES ('bannertext', '", $this.s($data.item('bannertext')), "', 'banner')")
                    $this.database.execute($query)
                    $query = -join("INSERT INTO Tbl_Settings (SETTING, SET_VALUE, CATEGORY) VALUES ('bordercolor', '", $this.s($data.item('bordercolor')), "', 'banner')")
                    $this.database.execute($query)
                    $query = -join("INSERT INTO Tbl_Settings (SETTING, SET_VALUE, CATEGORY) VALUES ('backgroundcolor', '", $this.s($data.item('backgroundcolor')), "', 'banner')")
                    $this.database.execute($query)
                    $query = -join("INSERT INTO Tbl_Settings (SETTING, SET_VALUE, CATEGORY) VALUES ('textcolor', '", $this.s($data.item('textcolor')), "', 'banner')")
                    $this.database.execute($query)
                }
                else{  #if records are returned, we'll update
                    $query = -join("UPDATE Tbl_Settings SET SET_VALUE='", $this.s($data.item('showbanner')), "' WHERE SETTING='showbanner' AND CATEGORY='banner'")
                    $this.database.execute($query)
                    $query = -join("UPDATE Tbl_Settings SET SET_VALUE='", $this.s($data.item('bannertext')), "' WHERE SETTING='bannertext' AND CATEGORY='banner'")
                    $this.database.execute($query)
                    $query = -join("UPDATE Tbl_Settings SET SET_VALUE='", $this.s($data.item('bordercolor')), "' WHERE SETTING='bordercolor' AND CATEGORY='banner'")
                    $this.database.execute($query)
                    $query = -join("UPDATE Tbl_Settings SET SET_VALUE='", $this.s($data.item('backgroundcolor')), "' WHERE SETTING='backgroundcolor' AND CATEGORY='banner'")
                    $this.database.execute($query)
                    $query = -join("UPDATE Tbl_Settings SET SET_VALUE='", $this.s($data.item('textcolor')), "' WHERE SETTING='textcolor' AND CATEGORY='banner'")
                    $this.database.execute($query)
                }
            }
            catch{
                $this.html = "failed"
            }
            if($this.html -ne "failed"){ $this.html = "success"}
        }
        
    }
    submitPost($data){
        $postId = $this.s($data.item('postId'))
        $postTitle = $this.s($data.item('title'))
        $postBody = $this.s($data.item('body'))
        $userId = $this.s($data.item('userId'))
        $timestamp = (Get-Date -Format g)
        $query = ""
        $RecSet = $null
        try{
            if($postId){
                $query = "SELECT * FROM Tbl_News WHERE ID = $postId"
                $RecSet = $this.database.openRecordSet($query, 3, 1)
            }
            if($RecSet.State -le 0 -or $postId -eq "" -or $postId -eq "0" -or $postId -eq 0){
                $query = "INSERT INTO Tbl_News (TITLE, BODY, AUTHOR, [TIMESTAMP]) VALUES ('$postTitle', '$postBody', '$userId', '$timestamp')"
                #Write-Host $query
                $this.database.execute($query)
            }
            else{
                $query = "UPDATE Tbl_News SET TITLE='$postTitle', BODY='$postBody', AUTHOR='$userId', [TIMESTAMP]='$timestamp' WHERE ID=$postId"
                $this.database.execute($query)
            }
        }
        catch [Exception]{
            Write-Host $_.Exception.GetType().FullName "/n" $_.Exception.Message "/n" $query
            $this.html = "failed"
        }
        if($this.html -ne "failed"){ $this.html = "success"}
        Start-Sleep -Seconds 1
    }
    submitModel($data){
        $id = $this.s($data.item('id'))
        $manufacturer = $this.s($data.item('manufacturer'))
        $model = $this.s($data.item('model'))
        $isLaptop = $this.s($data.item('islaptop'))
        $query = ""
        $RecSet = $null
        try{
            if($id){
                $query = "SELECT * FROM Tbl_Models WHERE ID = $id"
                $RecSet = $this.database.openRecordSet($query, 3, 1)
            }
            if($RecSet.State -le 0 -or $id -eq "" -or $id -eq "0" -or $id -eq 0){
                $query = "INSERT INTO Tbl_ComputerModels (MANUFACTURER, MODEL, IS_LAPTOP) VALUES ('$manufacturer', '$model', $isLaptop)"
                #Write-Host $query
                $this.database.execute($query)
            }
            else{
                $query = "UPDATE Tbl_ComputerModels SET MANUFACTURER='$manufacturer', MODEL='$model', IS_LAPTOP=$isLaptop WHERE ID=$id"
                $this.database.execute($query)
            }
        }
        catch [Exception]{
            Write-Host $_.Exception.GetType().FullName "`n" $_.Exception.Message "`n" $query
            $this.html = "failed"
        }
        if($this.html -ne "failed"){ $this.html = "success"}
        Start-Sleep -Seconds 1
    }
    submitReason($data){
        $id = $this.s($data.item('id'))
        $reason = $this.s($data.item('reason'))
        $sortorder = $this.s($data.item('sortorder'))
        $shortdesc = $this.s($data.item('shortdesc'))
        $longdesc = $this.s($data.item('longdesc'))
        $badexample = $this.s($data.item('badexample'))
        $goodexample = $this.s($data.item('goodexample'))
        $reimagereq = $this.s($data.item('reimagereq'))
        $query = ""
        $RecSet = $null
        try{
            if($id){
                $query = "SELECT * FROM Tbl_WOReasonList WHERE ID = $id"
                $RecSet = $this.database.openRecordSet($query, 3, 1)
            }
            if($RecSet.State -le 0 -or $id -eq "" -or $id -eq "0" -or $id -eq 0){
                $query = "INSERT INTO Tbl_WOReasonList (Reason, ReasonSortOrder, ShortDescription, IssueDescriptionHeader, IssueDescriptionBadEx, IssueDescriptionGoodEx, ReimageRequired) VALUES ('$reason', '$sortorder', '$shortdesc', '$longdesc', '$badexample', '$goodexample', $reimagereq)"
                #Write-Host $query
                $this.database.execute($query)
            }
            else{
                $query = "UPDATE Tbl_WOReasonList SET Reason='$reason', ReasonSortOrder='$sortorder', ShortDescription='$shortdesc', IssueDescriptionHeader='$longdesc', IssueDescriptionBadEx='$badexample', IssueDescriptionGoodEx='$goodexample', ReimageRequired=$reimagereq WHERE ID=$id"
                $this.database.execute($query)
            }
        }
        catch [Exception]{
            Write-Host $_.Exception.GetType().FullName "/n" $_.Exception.Message "/n" $query
            $this.html = "failed"
        }
        if($this.html -ne "failed"){ $this.html = "success"}
        Start-Sleep -Seconds 1
    }
    submitDirectorate($data){
        $id = $this.s($data.item('id'))
        $directorate = $this.s($data.item('directorate'))
        $code = $this.s($data.item('code'))
        $sortorder = $this.s($data.item('sortorder'))
        $query = ""
        $RecSet = $null
        try{
            if($id){
                $query = "SELECT * FROM Tbl_DirectorateCodes WHERE ID = $id"
                $RecSet = $this.database.openRecordSet($query, 3, 1)
            }
            if($RecSet.State -le 0 -or $id -eq ""){
                $query = "INSERT INTO Tbl_DirectorateCodes (DirectorateDesc, DirectorateCode, SortOrder) VALUES ('$directorate', '$code', '$sortorder')"
                #Write-Host $query
                $this.database.execute($query)
            }
            else{
                $query = "UPDATE Tbl_DirectorateCodes SET DirectorateDesc='$directorate', DirectorateCode='$code', SortOrder='$sortorder' WHERE ID=$id"
                $this.database.execute($query)
            }
        }
        catch [Exception]{
            Write-Host $_.Exception.GetType().FullName "/n" $_.Exception.Message "/n" $query
            $this.html = "failed"
        }
        if($this.html -ne "failed"){ $this.html = "success"}
        Start-Sleep -Seconds 1
    }
    submitRank($data){
        $id = $this.s($data.item('id'))
        $rank = $this.s($data.item('rank'))
        $branch = $this.s($data.item('branch'))
        $order = $this.s($data.item('order'))
        $query = ""
        $RecSet = $null
        try{
            if($id){
                $query = "SELECT * FROM Tbl_Ranks WHERE ID = $id"
                $RecSet = $this.database.openRecordSet($query, 3, 1)
            }
            if($RecSet.State -le 0 -or $id -eq ""){
                $query = "INSERT INTO Tbl_Ranks (RANK, BRANCH, [ORDER]) VALUES ('$rank', '$branch', $order)"
                #Write-Host $query
                $this.database.execute($query)
            }
            else{
                $query = "UPDATE Tbl_Ranks SET RANK='$rank', BRANCH='$branch', [ORDER]=$order WHERE ID=$id"
                $this.database.execute($query)
            }
        }
        catch [Exception]{
            Write-Host $_.Exception.GetType().FullName "/n" $_.Exception.Message "/n" $query
            $this.html = "failed"
        }
        if($this.html -ne "failed"){ $this.html = "success"}
        Start-Sleep -Seconds 1
    }
    submitAdmin($data){
        $this.html = ""
        foreach($id in $data.GetEnumerator()){
            $val = $this.database.sanitizeString($id.value)
            if($val.length -gt 0 -and $val -notcontains "*" -and $val -notcontains "%"){
                $query = "UPDATE Tbl_Users SET IsAdmin = true WHERE ID = $val"
                Write-Host "q = $query"
                $this.database.execute($query)
                # Check if delete was successful
                $query = "SELECT IsAdmin,F_NAME,MI,L_NAME FROM Tbl_Users WHERE ID = $val"
                $RecSet = $this.database.openRecordSet($query, 3, 1)
                $name = -join($RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("MI").Value, " ", $RecSet.Fields.Item("L_NAME").Value)                
                if($RecSet.Fields.Item("IsAdmin").value -eq 1){
                    $this.html += "$name is now an admin. "
                }
                else{
                    $this.html += "$name could not be made an admin. ";
                }
            }
            else{
                $this.html += "Invalid data received: $val.";
            }

        }
    }
    submitPassword($data){
        $id = $this.s($data.item('id'))
        $pw = $this.s($data.item('password'))
        $salt = -join ((48..57) + (65..90) + (97..122) | Get-Random -Count 64 | % {[char]$_})
        $saltedPw = -join($salt, $pw)
        $hash = -join($this.getStringHash($saltedPw, "SHA256"), $salt)
        $query = ""
        $RecSet = $null
        try{
            if($id){
                $query = "SELECT * FROM Tbl_Users WHERE ID = $id"
                $RecSet = $this.database.openRecordSet($query, 3, 1)
            }
            if($RecSet.State -le 0 -or $id -eq ""){
                $this.html = "failed"
            }
            else{
                $query = "UPDATE Tbl_Users SET [PASSWORD] = `"$hash`" WHERE ID = $id"
                $this.database.execute($query)
            }
        }
        catch [Exception]{
            Write-Host $_.Exception.GetType().FullName "\n" $_.Exception.Message "\n" $query
            $this.html = "failed"
        }
        if($this.html -ne "failed"){ $this.html = "success"}
        Start-Sleep -Seconds 1
    }
    login($data){
        $username = $this.s($data.item('username'))
        $password = $this.s($data.item('password'))
        $query = "SELECT * FROM Tbl_Users WHERE USERNAME = '$username'"
        try{
            if($password -and $username){
                $RecSet = $this.database.openRecordSet($query, 3, 1)
                if($RecSet.State -le 0){
                    $this.html = ""
                }
                else{    
                    $storedPass = $RecSet.Fields.Item("PASSWORD").Value
                    $salt = $storedPass.substring(64)
                    $saltedPw = -join($salt, $password)
                    $hash = -join($this.getStringHash($saltedPw, "SHA256"), $salt)
                    $name = -join($RecSet.Fields.Item("F_NAME").Value, " ", $RecSet.Fields.Item("MI").Value, " ", $RecSet.Fields.Item("L_NAME").Value)                    
                    if($hash -eq $storedPass){
                        $this.html = (Read-Html "admin-tabs.html" @{'name' = $name})
                    }
                    else{
                        $this.html = ""
                    }
                }
            }
            else{
                $this.html = ""
            }
        }
        catch [Exception]{
            Write-Host $_.Exception.GetType().FullName "\n" $_.Exception.Message "\n" $query
            $this.html = "failed"
        }

    }
    submitLocation($data){
        $query = "SELECT * FROM Tbl_Settings WHERE SETTING = `"workorder_path`""
        $RecSet = $this.database.openRecordSet($query, 3, 1)
        if($RecSet.State -gt 0){ # Make sure the record set opened
            try{
                if($RecSet.EOF){  # If 0 records are returned, we'll insert
                    $query = -join("INSERT INTO Tbl_Settings (SETTING, SET_VALUE, CATEGORY) VALUES ('workorder_path', '", $this.s($data.item('path')), "', 'workorders')")
                    $this.database.execute($query)
                }
                else{  #if records are returned, we'll update
                    $query = -join("UPDATE Tbl_Settings SET SET_VALUE='", $this.s($data.item('path')), "' WHERE SETTING='workorder_path' AND CATEGORY='workorders'")
                    $this.database.execute($query)
                }
            }
            catch{
                $this.html = "failed"
            }
            if($this.html -ne "failed"){ $this.html = "success"}
        }
        
    }
    testLocation($data){
        $this.html = ""
        $path = $this.s($data.item("path")).Replace('$SERVERDIR', $Global:serverPath)
        Write-Host "Testing path: $path"
        if(Test-Path $path){
            try{
                $textFile = Join-Path $path "test.txt"
                New-Item $textFile
                Set-Content $textFile "This is a test to see if this directory is writable for the G6 Help Desk work order server."
                Remove-Item $textFile
            }
            catch{
                $this.html = "failed"
            }
            $this.d($this.html, "success")
        }
        else{
            $this.html = "failed"
        }
    }


    [String] getHtml(){
        return $this.html
    }
    cleanup(){
        $this.database.cleanup()
    }
    [string] d($str, $def){
        # Sets a string to a default if it is empty
        if([String]::IsNullOrEmpty($str)){
            $str = $def
        }
        return $str
    }
    [string] s($str){
        # Convenience wrapper
        return $this.database.sanitizeString($str)
    }
    [string] formatPhone($str){
        if($str -match '\d{4,11}'){
            switch($str.length){
                11{
                    $str = $str.insert(1," ")
                    $str = $str.insert(4,"-")
                    $str = $str.insert(8,"-")
                }
                10{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                9{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                8{
                    $str = $str.insert(3,"-")
                    $str = $str.insert(7,"-")
                }
                7{
                    $str = $str.insert(3,"-")
                }
                6{
                    $str = $str.insert(3,"-")
                }
                5{
                    $str = $str.insert(3,"-")
                 }
                4{
                    $str = $str.insert(3,"-")
                }
            }
        }
        return $str
    }
    # https://gallery.technet.microsoft.com/scriptcenter/Get-StringHash-aa843f71
    # http://jongurgul.com/blog/get-stringhash-get-filehash/ 
    [string] getStringHash([String] $String, $HashName = "MD5") { 
        $StringBuilder = New-Object System.Text.StringBuilder 
        [System.Security.Cryptography.HashAlgorithm]::Create($HashName).ComputeHash([System.Text.Encoding]::UTF8.GetBytes($String))|%{ 
            [Void]$StringBuilder.Append($_.ToString("x2")) 
        }
        return $StringBuilder.ToString() 
    }
}