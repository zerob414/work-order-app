class Database{
    # ADO Connection States Constants
    [int] $adStateClosed = 0
    [int] $adStateOpen = 1
    [int] $adStateConnecting = 2
    [int] $adStateExecuting = 4
    [int] $adStateFetching = 8
    
    $OpenStatic = 3       #Opens with a static recordset, creates a readonly copy in the computer's memory.
    $OpenDynamic = 2      #Allows moving through all records.

    $LockOptimistic = 3   #LockOptimistic locks only the record being modified at the moment of modification.
    $LockPessimistic = 2  #LockPessimistic locks only the record being modified before and during modification, prevents concurrent modifications.
    $LockReadOnly = 1     #Just like it sounds...

    hidden [String] $databasePath
    hidden $connection = $null

    Database(){
        $this.databasePath = ".\db\WorkOrderDB.accdb"
        $this.connectToDatabase($this.databasePath)
    }
    Database($dbPath){
        $this.databasePath = $dbPath
        $this.connectToDatabase($this.databasePath)
    }

    connectToDatabase($Db){
        $this.databasePath = $Db
        $this.connection = New-Object -ComObject ADODB.Connection
        $this.connection.Open("Provider = Microsoft.ACE.OLEDB.12.0;Data Source='$Db'" ) #The ACE engine is provided by MS Office (Access to be exact). If office is not installed, or if this script is run in a different architecture (32/64 bit) than Office, the engine will not be found.
        if($this.connection -eq $NULL -OR $this.connection.State -eq $this.adStateClosed){
            Throw "$this.DatabasePath found but a valid connection could not be made."
        }
    }

    [object] getConnection(){
        return $this.connection
    }

    [object] openRecordSet($query, $cursorType, $lockType){
        $RecordSet = new-object -ComObject ADODB.Recordset
        Write-Host "query = $query"   #Run script with -debug switch or change to Write-Host to see the exact query in the console.
        $RecordSet.Open($query, $this.connection, $cursorType, $lockType)
        return $RecordSet
    }
    [object] openRecordSet($query, $cursorType){
        $a = $this.openRecordSet($query, $cursorType, $this.LockPessimistic)
        Write-Host "a = $a"
        return $a
    }
    [object] openRecordSet($query){
        return $this.openRecordSet($query, $this.OpenDynamic, $this.LockPessimistic)
    }
    execute($query){
        $this.connection.Execute($query)
    }

    [string] sanitizeString([string] $in){
        $disallowedChars = @() # Add characters you want removed from strings here.
        $escapeChars = @('"', "'") # Add characters you want escaped in strings here.
        foreach ($ch in $disallowedChars) {
            $in = $in.Replace($ch, "")
        }
        foreach ($ch in $escapeChars) {
            $in = $in.Replace($ch, -join($ch, $ch)) # In MS Access, escaping characters is done by doubling them.
        } 
        #$in = $in.Replace("'", "&#39;")
        #$in = $in.Replace('"', '&#34;')

        return $in
    }

    cleanup(){
        $this.connection.Close()
    }
}