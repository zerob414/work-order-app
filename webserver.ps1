Using Module .\modules\userprofile.psm1
Using Module .\modules\computer.psm1
Using Module .\modules\admin.psm1
Using Module .\modules\workorder.psm1

# This is the base URL
$Global:baseurl = 'http://localhost:8080/'


# This is the location of the server. (necessary because $PSScriptRoot returns the location of the module file when referenced in a module)
$Global:serverPath = $PSScriptRoot

# This is the relative location of the database file
$databasePath = ".\db\WorkOrderDB.accdb"

# Start the web server
# Note that inbound traffic on port 8080 should be blocked by the firewall so that this server is only available on localhost
$listener = New-Object System.Net.HttpListener
$listener.Prefixes.Add($Global:baseurl)
$listener.Start()
$arch = if([System.Environment]::Is64BitProcess){"x64"} else {"x86"}
Write-Host "Webserver started. ($arch)"

##############################
#.SYNOPSIS
#The webserver application for the Help Desk Work Order App.
#
#.DESCRIPTION
#This script provides a web server running on port 8080 for the Help Desk Work
#Order App. It serves the HTML/CSS/JS front end while processing the data on
#the backend. A MS Access database (32bit arch) is used for persistant storage.
#.NET classes are tapped into for providing much of the funtionality. This
#app is meant to be available only on localhost and to only have a single user
#at any given time. It was developed in PowerShell with an HTML front end due
#to limitations of what was available in the environment I was developing in.
#For example, I was not allowed to install Java IDEs and Visual Studio would
#not install properly (I still don't know why, the installer would just hang
#forever), or else I would have written this is Java or C# with a desktop
#frontend (JavaFX or WPF).
#
#.PARAMETER Response
#none
#
#.PARAMETER message
#none
#
#.EXAMPLE
#webserver.ps1
#
#.NOTES
#The 32bit version of the MS Access driver (JET) is required to be installed
#and this script must be executed in 32bit PowerShell.
################################ 

##Functions
function error404($Response, $url, $message = ""){
  $Response.statuscode = 404
  return Read-Html "error404.html" (@{message = $message; url = $url})
}

function Read-Html($path, $variables){
  if(-not $path.StartsWith('/') -and -not $path.StartsWith('\')){$path = '/' + $path} # Make sure there is a slash at the beginning
  $html = [IO.File]::ReadAllText(".\html$path")

  if($html.Contains('<import ')){
    $imports = ([regex]'<import .*>').Matches($html)
    foreach($import in $imports){
      $importPath = [regex]::Match($import,'(src=")(.*)(")').Captures.Groups[2].Value
      $importHtml = Read-Html($importPath)
      $html = $html.Replace($import, $importHtml)
    }
  }

  if($variables -and $variables -is [hashtable]){
    foreach($kvp in $variables.GetEnumerator()){
      $var = $kvp.Key
      $val = $kvp.Value
      $html = $html.Replace("`$$var","$val")
    }
  }
  return $html
}

function Get-MimeType($path){
  $imageTypes = @("jpg","jpeg","gif","png","tiff","svg","bmp","ico","woff","woff2","ttf")
  $applicationTypes = @("pdf")
  $audioTypes = @()
  $textTypes = @("html","htm","css","js","txt","csv")
  
  $extension = [System.IO.Path]::GetExtension($path).Replace('.','')

  if($imageTypes -contains $extension){ return "image/$extension" }
  elseif($applicationTypes -contains $extension){ return "application/$extension" }
  elseif($audioTypes -contains $extension){ return "audio/$extension" }
  elseif($textTypes -contains $extension){ return "text/$extension" }
  else { return "text/plain" }
}

function Parse-Query($urlQuery){
  if($urlQuery.Length -gt 0){
    $urlQuery = $urlQuery.Replace('?', '')
    $queryParams = @{ originalQuery = $urlQuery }
    $parts = $urlQuery.Split('&')
    foreach($part in $parts){
      $pieces = $part.Split('=')
      $queryParams.add($pieces[0], $pieces[1])
    }
    return $queryParams
  }
}

# get post data from the input stream. https://hinchley.net/articles/create-a-web-server-using-powershell/
function extractPost($request) {
  $length = $request.contentlength64
  $buffer = new-object "byte[]" $length

  [void]$request.inputstream.read($buffer, 0, $length)
  $body = [system.text.encoding]::ascii.getstring($buffer)
  #$body = [system.Web.HttpUtility]::UrlDecode($body)

  $data = @{}
  $body.split('&') | ForEach-Object{
    $part = $_.split('=')
    $data.add([system.Web.HttpUtility]::UrlDecode($part[0]), [system.Web.HttpUtility]::UrlDecode($part[1]))
  }

  return $data
}
function Get-NavHashtable($location){
  $hash = @{atHome = ""; atWorkOrder = ""; atComputer = ""; atProfile = ""}
  switch($location){
    "home"{
      $hash.atHome = "nav-button-selected"
    }
    "profile"{
      $hash.atProfile = "nav-button-selected"
    }
    "computer"{
      $hash.atComputer = "nav-button-selected"
    }
    "workorder"{
      $hash.atWorkOrder = "nav-button-selected"
    }
  }
  return $hash
}

# Server
$terminateServer = $false
try {
  while ($listener.IsListening) {  
    # process received request
    $context = $listener.GetContext()
    $Request = $context.Request
    $Response = $context.Response

    $received = '{0} {1}' -f $Request.httpmethod, $Request.url.localpath
    $parameters = Parse-Query $Request.Url.Query
    Write-Output "Request Received: "$Request.Url.OriginalString
    
    $html = $null
    $mimeType = "text/html"

    # Map requests to functions or pages here.
    switch($received){
      "GET /"{  # index page
        $navHash = Get-NavHashtable "home"
        $html = (Read-Html "index.html" $navHash)
      }

      "GET /profile"{ # Show the user profile page
        $navHash = Get-NavHashtable "profile"
        if($parameters.Count -gt 1){
          [UserProfile] $theProfile = [UserProfile]::new($databasePath, $parameters)
          $html = $theProfile.getHtml()
          $theProfile.cleanup()
        }
        else {
          $html = (Read-Html "profile.html" $navHash)
        }
      }

      "POST /profile"{ # Create or edit a profile
        [UserProfile] $theProfile = [UserProfile]::new($databasePath)
        $data = extractPost $Request
        $theProfile.updateUser($data)
        $html = $theProfile.getHtml()
      }

      "GET /computer"{ # Show the computer profile page
        $navHash = Get-NavHashtable "computer"
        if($parameters.Count -gt 1){
          [ComputerProfile] $theProfile = [ComputerProfile]::new($databasePath, $parameters)
          $html = $theProfile.getHtml()
          $theProfile.cleanup()
        }
        else {
          $html = (Read-Html "computer.html" $navHash)
        }
      }

      "POST /computer"{ # Create or edit a profile
        [ComputerProfile] $theProfile = [ComputerProfile]::new($databasePath)
        $data = extractPost $Request
        $theProfile.updateComputer($data)
        $html = $theProfile.getHtml()
      }

      "GET /workorder"{ # Show the work order page
        $navHash = Get-NavHashtable "workorder"
        if($parameters.Count -gt 1){
          [WorkOrders] $theWorkOrders = [WorkOrders]::new($databasePath, $parameters)
          $html = $theWorkOrders.getHtml()
          $theWorkOrders.cleanup()
        }
        else {
          $html = (Read-Html "workorder.html" $navHash)
        }
      }

      "POST /workorder"{ # Create or edit work order data
        [WorkOrders] $theWorkOrders = [WorkOrders]::new($databasePath)
        $data = extractPost $Request
        $theWorkOrders.updateData($parameters, $data)
        $html = $theWorkOrders.getHtml()
      }

      "GET /workordernew"{ # Show the work order page
        $navHash = Get-NavHashtable "workorder"
        if($parameters.Count -gt 1){
          [WorkOrders] $theWorkOrders = [WorkOrders]::new($databasePath, $parameters)
          $html = $theWorkOrders.getHtml()
          $theWorkOrders.cleanup()
        }
        else {
          $html = (Read-Html "workordernew.html" $navHash)
        }
      }

      "POST /workordernew"{ # Create or edit work order data
        [WorkOrders] $theWorkOrders = [WorkOrders]::new($databasePath)
        $data = extractPost $Request
        $theWorkOrders.updateData($data)
        $html = $theWorkOrders.getHtml()
      }


      "GET /admin"{ # Show the admin page
        $navHash = Get-NavHashtable "admin"
        if($parameters.Count -gt 1){
          [Admin] $theMod = [Admin]::new($databasePath, $parameters)
          $html = $theMod.getHtml()
          $theMod.cleanup()
        }
        else {
          $html = (Read-Html "admin.html" $navHash)
        }
      }

      "POST /admin"{ # Update stuff on the admin page
        [Admin] $theMod = [Admin]::new($databasePath)
        $data = extractPost $Request
        $theMod.updateData($parameters, $data)
        $html = $theMod.getHtml()
      }

      "GET /exit"{  # Terminate the web server
        $html = Read-Html("exited.html")
        $terminateServer = $true
      }

      default{
        $path = '{0}' -f $Request.url.localpath
        if(-not $path.Contains("..")){ # Prevent directory traversal
          if(Test-Path "$PSScriptRoot\html$path"){ # See if this path exists
            if((Get-Item "$PSScriptRoot\html$path") -is [System.IO.DirectoryInfo]){  # Check if path is a directory
              if(Test-Path "$PSScriptRoot\html$path\index.html"){  # See if the directory has an index file
                $html = Read-Html "$PSScriptRoot\html$path\index.html" # return the index.html file
              }
            }
            else{
              $mimeType = Get-MimeType $path
              $html = Read-Html $path
            }
          }
        }
      }
    }

    if ($html -eq $null) {
      $html = error404 $Response $Request.Url.OriginalString
    } 
    
    # Respond to the request
    $buffer = "Oops, no content!"
    if($mimeType.StartsWith("text")){
      $buffer = [Text.Encoding]::UTF8.GetBytes($html)
    }
    else{
      $buffer = [System.IO.File]::ReadAllBytes("$PSScriptRoot\html$path")
    }
    $Response.ContentType = $mimeType
    $Response.ContentLength64 = $buffer.length
    $Response.OutputStream.Write($buffer, 0, $buffer.length)
  
    $Response.Close()

    if($terminateServer){ $listener.Stop() }
  }
}
finally {
  $listener.Stop()
}